
###################################################################################################
# Script 1

#Folder path for pipeline
arg1=/pasteur/entites/GGS/CMS/runCMS

#Folder for analysis
arg2=/pasteur/entites/GGS/CMS/runCMS/test

#Input phenotypes file
arg3=/pasteur/entites/GGS/CMS/runCMS/test/test_phenotypes.txt

#Input summary file
arg4=/pasteur/entites/GGS/CMS/runCMS/test/test_summary.csv

#Covariates number
arg5=5

#Threshold on outcome variance explained by covariates
arg6=0.7

Rscript ./runCMS/Script1_data_preparation.R $arg1 $arg2 $arg3 $arg4 $arg5 $arg6

###################################################################################################
# Script 2

#Folder path for pipeline
arg1=/pasteur/entites/GGS/CMS/runCMS

#Folder for analysis
arg2=/pasteur/entites/GGS/CMS/runCMS/test

#Genotypes file
arg3=/pasteur/entites/GGS/CMS/runCMS/test/test_genotypes.raw

#Input summary file
arg4=/pasteur/entites/GGS/CMS/runCMS/test/test_summary.csv

#SNP number
arg5=100

#Number of SNPs per block
arg6=10
sbatch --array=1-10 --qos=fast ./runCMS/Script2_launch_analysis_SLURM.sh $arg1 $arg2 $arg3 $arg4 $arg5 $arg6

#
###################################################################################################
# Script 3

#Folder path for pipeline
arg1=/pasteur/entites/GGS/CMS/runCMS


#Folder for analysis
arg2=/pasteur/entites/GGS/CMS/runCMS/test

#Genotypes file
arg3=/pasteur/entites/GGS/CMS/runCMS/test/test_genotypes.bim

#Input summary file
arg4=/pasteur/entites/GGS/CMS/runCMS/test/test_summary.csv

#Blocks number
arg5=10
###################################################################################################
#Significance threshold
arg6=0.1

python3 ./runCMS/Script3_results_analysis.py $arg1 $arg2 $arg3 $arg4 $arg5 $arg6
