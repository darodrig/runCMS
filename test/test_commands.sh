
###################################################################################################
# Script 1

#Folder path for pipeline
arg1=/u/project/pajukant/agallois/runCMS

#Folder for analysis
arg2=/u/project/pajukant/agallois/test

#Input phenotypes file
arg3=/u/project/pajukant/agallois/test/test_phenotypes.txt

#Input summary file
arg4=/u/project/pajukant/agallois/test/test_summary.csv

#Covariates number
arg5=5

#Threshold on outcome variance explained by covariates
arg6=0.7

Rscript Script1_data_preparation.R $arg1 $arg2 $arg3 $arg4 $arg5 $arg6


###################################################################################################
# Script 2

#Folder path for pipeline
arg1=/u/project/pajukant/agallois/runCMS

#Folder for analysis
arg2=/u/project/pajukant/agallois/test

#Genotypes file
arg3=/u/project/pajukant/agallois/test/test_genotypes.raw

#Input summary file
arg4=/u/project/pajukant/agallois/test/test_summary.csv

#SNP number
arg5=100

#Number of SNPs per block
arg6=10

 sbatch --array=1-10 Script2_launch_analysis.sh $arg1 $arg2 $arg3 $arg4 $arg5 $arg6


###################################################################################################
# Script 3

#Folder path for pipeline
arg1=/u/project/pajukant/agallois/runCMS


#Folder for analysis
arg2=/u/project/pajukant/agallois/test

#Genotypes file
arg3=/u/project/pajukant/agallois/test/test_genotypes.bim

#Input summary file
arg4=/u/project/pajukant/agallois/test/test_summary.csv

#Blocks number
arg5=10

#Significance threshold
arg6=0.1

python3 Script3_results_analysis.py $arg1 $arg2 $arg3 $arg4 $arg5 $arg6
