#!/bin/sh

#$ -o /u/project/pajukant/agallois/test/log_output
#$ -e /u/project/pajukant/agallois/test/log_error
#$ -t 1:10
#$ -l h_data=7G,h_rt=24:00:00

. /u/local/Modules/default/init/modules.sh
module load python/3.6.1
module load R/3.2.1

pipeline_directory=$1
analysis_directory=$2
genotypes_file=$3
summary_file=$4
SNP_number=$5
block_size=$6

i=${SGE_TASK_ID}

SNP1=`echo "${block_size}*(${i}-1)+1" | bc -l`
SNP2=`echo $((${block_size}*${i}<${SNP_number}?${block_size}*${i}:${SNP_number}))`

python3 ${pipeline_directory}/others/Script2.1_create_data_file.py ${pipeline_directory} ${analysis_directory} ${genotypes_file} ${summary_file} ${i} ${SNP1} ${SNP2}
